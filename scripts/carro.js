
//Antonio defez 
"use strict;"
/*Variables globales para controlar el movimiento de la caja */
var posicion_actual=0;
var posicion_Maxima;
var posicion_Minima=0;
var ancho_actual;


//obtengo el stock actual de un articulo
function GetStock(articulo)
{
    var stock = articulo.querySelector(".stock").firstChild.textContent;
    var valores = stock.split(" ");
    var actual_stock = valores[valores.length-1];
    //console.log(actual_stock);
    return parseInt(actual_stock);
}

//obtengo el precio de un articulo
function GetPrecio(articulo)
{
    var price = articulo.querySelector(".price").firstChild.textContent;
    var valores = price.split(" ");
    var actual_precio = valores[0];
    //console.log(actual_precio);
    return parseInt(actual_precio);
}

//actualizo el stock de un item
function ActualizarStock(articulo,increment)
{
    var stock = GetStock(articulo);
    var nstock = stock+increment;
    if(nstock>=0)
    {
      articulo.querySelector(".stock").firstChild.textContent="Stock "+nstock;
      articulo.querySelector(".stock").className = " stock";
    }
    else
    {  
      articulo.querySelector(".stock").className += " agotado";
    }
}

function ActualizarObjetosComprados(articulo,increment)
{
     var objeto_compadros = parseInt(document.getElementById('citem').value);
     var articulos_actuales_comprados= objeto_compadros+increment;
     document.getElementById('citem').value=articulos_actuales_comprados;
     return articulos_actuales_comprados;
}

function ActualizarPrecio_total(articulo,increment)
{
    var total = parseInt(document.getElementById('cprice').value);
     total = total+(GetPrecio(articulo))*increment;
     document.getElementById('cprice').value=total+" €";
}

function ActualizarValores(articulo,increment)
{
     var btn_clear=document.getElementById('btn_clear');
     var btn_comprar=document.getElementById('btn_comprar');
     var carrito= document.getElementById('cart_items');
     
     var articulos_actuales_comprados=ActualizarObjetosComprados(articulo,increment);
     ActualizarPrecio_total(articulo,increment);
     EstadoBoton(btn_clear,true);
     EstadoBoton(btn_comprar,true);
     if( ComprobarNuevoArticulo(articulo))
         {  
             CambioTamañoCarro(increment);
         }
     var articulos_comprados=carrito.getElementsByClassName('item');
     if( articulos_comprados.length >=4 )
     {
         EstadoBoton(btn_next,true);
         EstadoBoton(btn_prev,true);       
     }
     else
     {
        CambioTamañoCarro(-1);
        EstadoBoton(btn_next,false);
        EstadoBoton(btn_prev,false);
        if(articulos_actuales_comprados==0)
        {
            EstadoBoton(btn_clear,false);
            EstadoBoton(btn_comprar,false);
        }
        else
        {
            EstadoBoton(btn_clear,true);
            EstadoBoton(btn_comprar,true);
        }
     }
}


function SacarIdOriginal( id_copia)
{
    var id_original= id_copia.substring(0,id_copia.length-1);
    return id_original;
}

//borrado de objeto desde flecha
function borrado_Delete(event)
{
   var flecha_articulo = event.currentTarget;
   console.log("borrando desde x-->"+flecha_articulo.parentNode.id);
   var articulo=document.getElementById(flecha_articulo.parentNode.id);
   borradoFinal(articulo);  
}
/*Recibe un articulo y lo borra */
function borradoFinal(articulo)
{
   var id_original = SacarIdOriginal(articulo.id);
   var articulo_original = document.getElementById(id_original);
   
   var carrito = document.getElementById('cart_items');
   /*borrando el articulo de compra */
   var cantidad = GetValueInput(articulo_original);
   carrito.removeChild(articulo);
   ActualizarValores(articulo,-1*cantidad);
   ActualizarStock(articulo_original,cantidad);
   posicion_actual=0;
   carrito.style.left=posicion_actual+"px";

   //comprobando el numero de articulos
   var cantidad_comprados=carrito.getElementsByClassName('item');;
   console.log("borrado desde aqui");
   if( cantidad_comprados.length ==4 )
     {
         EstadoBoton(btn_next,false);
         EstadoBoton(btn_prev,false);       
     }
}   

//borrado desde el  menos
function borrado_Minus(event)
{
   //Saco el elemento padre
   var flecha_articulo = event.currentTarget;
   var articulo_copia=document.getElementById(flecha_articulo.parentNode.id);
   var id_original=SacarIdOriginal(articulo_copia.id);
   var articulo_original = document.getElementById(id_original);

   if( GetValueInput(articulo_original)-1>0 )
   {
        //borramos solamente un articulo
        ActualizarStock(articulo_original,1);  
        ActualizarStockComprados(articulo_original,-1);
        ActualizarObjetosComprados(articulo_original,-1);
        ActualizarPrecio_total(articulo_original,-1);
        //cambiamos el estado del boton minus
        var boton = document.getElementById(flecha_articulo.parentNode.id+"a");
        boton.style.display="inline";
   }
   else
   {
       //borramos el primer articulo
       borradoFinal(articulo_copia);
   }
}

//Añadiendo el boton de borrar
function AñadirDelete()
{
    /*Añadiendo el elemento*/
    /*creando enlace delete con imagen*/
    var delete_link=document.createElement("A");
    delete_link.className = "delete";
    /*redefinimos el metodo borrado de la imagen*/
    delete_link.onclick = borrado_Delete;
    return delete_link;
}
//añadiendo el input
function AñadirCantidad(CopiaArticulo)
{
    var cantidad=document.createElement("INPUT");
    
    cantidad.className="cantidad";
    cantidad.id=CopiaArticulo+"c";
    cantidad.value=1;
    cantidad.readOnly="true";
    return cantidad;
}

//añadiendo el minus
function AñadirMinus()
{
    var minus_link=document.createElement("A");
    minus_link.className = "minus";
    minus_link.onclick=borrado_Minus;
    return minus_link;
}
//añadiendo el add
function AñadirAdd(CopiaArticulo)
{
    var add_link=document.createElement("A");
    add_link.className = "add";
    id_original=SacarIdOriginal(CopiaArticulo.id);
    articulo=document.getElementById(id_original);
    add_link.id=CopiaArticulo.id+"a";
    add_link.onclick=Add_copiaItem;
    return add_link;
}

function modificarCopiaArticulo(CopiaArticulo)
{
    //redifinimos el id
    CopiaArticulo.id=CopiaArticulo.id+"c";
    /*Desaparecer el stock */
    CopiaArticulo.querySelector(".stock").style.display = "none";
    /*Creando el aspa */
    var delete_link=AñadirDelete();

    /*Creando el input de cantidad*/
    var cantidad = AñadirCantidad(CopiaArticulo.id);

    /*Creando enlace con minus*/
    var minus_link=AñadirMinus(CopiaArticulo);
 
    /*Creando enlace con add*/
    var add_link =AñadirAdd(CopiaArticulo);

    CopiaArticulo.prepend(minus_link);
    CopiaArticulo.prepend(add_link);
    CopiaArticulo.prepend(cantidad);
    CopiaArticulo.prepend(delete_link);
}

function Add_copiaItem(event)
{
    var Copiarticulo = event.currentTarget.parentNode;
    var id_original=SacarIdOriginal(Copiarticulo.id);
    
    var articulo_original=document.getElementById(id_original);

    if(GetStock(articulo_original)!=0)
    {
         ActualizarStock(articulo_original,-1);
         //incrementar el numero de articulos 
         ActualizarObjetosComprados(articulo_original,1);
         ActualizarPrecio_total(articulo_original,1);
         ActualizarStockComprados(articulo_original,1);
    }
    else
    {
       ActualizarStock(articulo_original,-1);
       event.currentTarget.style.display="none";
    }
}


function copiaitem(event)
{
    var articulo = event.currentTarget;
    var actual_stock=GetStock(articulo);

    if(actual_stock!=0)
    {
        if( ComprobarNuevoArticulo(articulo))
        {
            ActualizarStock(articulo,-1);
            ActualizarValores(articulo,1);
            var carrito = document.getElementById('cart_items');
            var CopiaArticulo = articulo.cloneNode(true);
            modificarCopiaArticulo(CopiaArticulo);
            carrito.prepend(CopiaArticulo);
            articulo.className = "item";
        }
        else
        {
        //ya metido
            ActualizarStock(articulo,-1);
            ActualizarValores(articulo,1);
            console.log("Articulo ya introducido");
            ActualizarStockComprados(articulo,1);
        }
    }
    else
    {
        //ponemos el style de agotado
          ActualizarStock(articulo,-1);
    }  
}

function GetValueInput(articulo_original)
{
    console.log("input "+articulo_original.id);
    var input = articulo_original.id+"c"+"c";
    var input2=document.getElementById(input);
    var actual_valur=parseInt(input2.value);
    return actual_valur;
}

function ActualizarStockComprados(articulo,increment)
{
    var input=articulo.id+"c"+"c";
    var input2=document.getElementById(input);
    actual_valur=GetValueInput(articulo);
    input2.value=actual_valur+parseInt(increment);
}

function LimpiarCarro (event)
{
    var a=document.getElementById('cart_items');
    while(a.hasChildNodes())
    {
        var id_art=a.firstChild.id;
        var articulo = document.getElementById(id_art);
        borradoFinal(articulo);
    }
}

function CambioTamañoCarro(increment)
{

    var car_items=document.getElementById('cart_items');
    var actual_width=cart_items.offsetWidth;
    var total=actual_width+(increment)*125;
    if(total>=483)
    {
        posicion_Maxima=total;
        total+="px";
        cart_items.style.width=total;      
    }
    else
    {
        total=483;
        posicion_Maxima=total;
        cart_items.style.width=total+"px";
    }
    ancho_actual=posicion_Maxima;
}

function MoverCajaNext()
{   
    if(posicion_actual+ancho_actual >  500)
    {
       posicion_actual-=20;
       cart_items.style.left=posicion_actual+"px";
    }
}

function MoverCajaPrev()
{
    if(posicion_actual +20 < posicion_Minima)
    {
        posicion_actual+=20;
        cart_items.style.left=posicion_actual+"px";
    }
    else
    {
        posicion_actual=0;
        cart_items.style.left=posicion_actual+"px";
    }
}
    
function onWindowLoad()
{
    //declarando array de articulos
    var items = document.getElementsByClassName('item');
    for (var i = 1; i <= 5; i++)
    {
        items[i-1] = document.getElementById("i" + i);
        items[i-1].ondblclick = copiaitem;
    }
    //declaro los botones para los carritos
    var btn_clear=document.getElementById('btn_clear');
    var btn_next=document.getElementById('btn_next');
    var btn_comprar=document.getElementById('btn_comprar');
    var btn_prev=document.getElementById('btn_prev');
    btn_prev.addEventListener("click",MoverCajaPrev);
    btn_clear.addEventListener("click",LimpiarCarro);
    btn_next.addEventListener("click",MoverCajaNext);

    //actualizo el estado de los botones
    EstadoBoton(btn_next,false);
    EstadoBoton(btn_prev,false);
    EstadoBoton(btn_clear,false);
    EstadoBoton(btn_comprar,false);
    IncrustarEstilo();

}

function EstadoBoton(boton, visible)
{
    if(visible)
        boton.style.display="inline";
    else
        boton.style.display="none";
}

function IncrustarEstilo()
{
   //incrustando estilo en HTML
   var styleNode = document.createElement('style');
   styleNode.type = "text/css";

    var styleDelete = document.createTextNode('.delete{background:url(img/delete.png);}');
    var styleDelete_hover = document.createTextNode(
        '.delete:hover{background-image:url(img/delete-hover.png); }');
        
    styleNode.appendChild(styleDelete);
    styleNode.appendChild(styleDelete_hover);

    var styleMinus = document.createTextNode('.minus{background:url(img/minus.png);}');
    var styleMinus_hover = document.createTextNode(
            '.minus:hover{background-image:url(img/minus-hover.png); }');
    
    styleNode.appendChild(styleMinus);
    styleNode.appendChild(styleMinus_hover);

    var styleAdd = document.createTextNode('.add{background:url(img/add.png);}');
    var styleAdd_hover = document.createTextNode(
            '.add:hover{background-image:url(img/add-hover.png); }');
    
    styleNode.appendChild(styleAdd);
    styleNode.appendChild(styleAdd_hover);
   
   document.getElementsByTagName('head')[0].appendChild(styleNode);
}

function ComprobarNuevoArticulo(articulo)
{
    var articulo_copia_id=articulo.id+"c";
     var carrito= document.getElementById('cart_items');
     var articulos=carrito.getElementsByClassName('item');

     for(var i=0;i<articulos.length;i++)
     {
        if(articulos[i].id==articulo_copia_id)
        return false;
     }
     return true;
}
window.addEventListener("load",onWindowLoad);